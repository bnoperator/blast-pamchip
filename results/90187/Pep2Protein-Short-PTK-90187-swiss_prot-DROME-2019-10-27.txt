Sequence	PepProtein_SeqMatch	ID	PepProtein_UniprotACC	PepProtein_UniprotName	PepProtein_UniprotID	PepProtein_SeqHomology	PepProtein_SeqIdentity	PepProtein_PhosSite	PepProtein_Residue	PepProtein_PhosLink
TEASGYISSLEYP	DASGYISSAE	C1R_199_211	MLC2_DROME	MLC2	P54357	0.77	0.62	99	Y	P54357_99
KDGNGYISAAELR	KDASGYISSAELR	CALM_95_107	MLC2_DROME	MLC2	P54357	1	0.77	99	Y	P54357_99
KDGNGYISAAELR	KDGNGYIEGTEL	CALM_95_107	CAB32_DROME	CAB32	P41044	0.92	0.69	54	Y	P41044_54
KDGNGYISAAELR	KEGNGYITTGVLR	CALM_95_107	TNNC1_DROME	TNNC1	P47947	1	0.62	103	Y	P47947_103
KDGNGYISAAELR	DGNGYIS	CALM_95_107	NCAH_DROME	NCAH	P42325	0.54	0.54	115	Y	P42325_115
EKIGEGTYGVVYK	EKIGEGTYGVVYK	CDK2_8_20	CDK1_DROME	CDK1	P23572	1	1	15	Y	P23572_15
EKIGEGTYGVVYK	EKIGEGTYGVVYK	CDK2_8_20	CDK1_DROME	CDK1	P23572	1	1	19	Y	P23572_19
EKIGEGTYGVVYK	EKIGEGTYGIVYK	CDK2_8_20	CDK2_DROME	CDK2	P23573	1	0.92	19	Y	P23573_19
EKIGEGTYGVVYK	EKIGEGTYGIVYK	CDK2_8_20	CDK2_DROME	CDK2	P23573	1	0.92	23	Y	P23573_23
EKIGEGTYGVVYK	EKIGEGTYGTVFK	CDK2_8_20	CDK5_DROME	CDK5	P48609	1	0.85	15	Y	P48609_15
EKIGEGTYGVVYK	IGEGTYGQVYK	CDK2_8_20	CDK12_DROME	CDK12	Q9VP22	0.85	0.77	815	Y	Q9VP22_815
EKIGEGTYGVVYK	IGEGTYGQVYK	CDK2_8_20	CDK12_DROME	CDK12	Q9VP22	0.85	0.77	819	Y	Q9VP22_819
EKIGEGTYGVVYK	IEEGTYGVVY	CDK2_8_20	KP58_DROME	KP58	Q9VPC0	0.77	0.69	569	Y	Q9VPC0_569
EKIGEGTYGVVYK	IEEGTYGVVY	CDK2_8_20	KP58_DROME	KP58	Q9VPC0	0.77	0.69	573	Y	Q9VPC0_573
EKIGEGTYGVVYK	KLGEGSYGSVYK	CDK2_8_20	HIPPO_DROME	HIPPO	Q8T0S6	0.92	0.69	53	Y	Q8T0S6_53
EKIGEGTYGVVYK	KLGEGSYGSVYK	CDK2_8_20	HIPPO_DROME	HIPPO	Q8T0S6	0.92	0.69	57	Y	Q8T0S6_57
EKIGEGTYGVVYK	EKVGHGSYGVVCK	CDK2_8_20	M3K7_DROME	M3K7	Q9V3Q6	1	0.69	30	Y	Q9V3Q6_30
EKIGEGTYGVVYK	IGEGSYSIVY	CDK2_8_20	PDPK1_DROME	PDPK1	Q9W0V1	0.77	0.54	257	Y	Q9W0V1_257
EKIGEGTYGVVYK	IGEGSYSIVY	CDK2_8_20	PDPK1_DROME	PDPK1	Q9W0V1	0.77	0.54	261	Y	Q9W0V1_261
EKIGEGTYGVVYK	KIGEGAYGEVF	CDK2_8_20	HASP_DROME	HASP	P83103	0.85	0.62	259	Y	P83103_259
EKIGEGTYGVVYK	KVGRGTYGHVYK	CDK2_8_20	CDK8_DROME	CDK8	Q9VT57	0.92	0.69	32	Y	Q9VT57_32
EKIGEGTYGVVYK	KVGRGTYGHVYK	CDK2_8_20	CDK8_DROME	CDK8	Q9VT57	0.92	0.69	36	Y	Q9VT57_36
CQLGQRIYQYIQS	CQLGQRIYHYIQS	DYR1A_312_324	MNB_DROME	MNB	P49657	1	0.92	324	Y	P49657_324
CQLGQRIYQYIQS	CQLGQRIYHYIQS	DYR1A_312_324	MNB_DROME	MNB	P49657	1	0.92	326	Y	P49657_326
CQLGQRIYQYIQS	QRIYTYIQS	DYR1A_312_324	DYRK3_DROME	DYRK3	P83102	0.69	0.62	434	Y	P83102_434
CQLGQRIYQYIQS	QRIYTYIQS	DYR1A_312_324	DYRK3_DROME	DYRK3	P83102	0.69	0.62	436	Y	P83102_436
LNQGVRTYVDPFT	GVRTYVD	EPHA4_589_601	AMYB_DROME	AMYB	P81641	0.54	0.54	110	Y	P81641_110
LNQGVRTYVDPFT	GVRTYVD	EPHA4_589_601	AMYA_DROME	AMYA	P08144	0.54	0.54	110	Y	P08144_110
TYIDPETYEDPNR	DPETYED	EPHA7_607_619	ATU_DROME	ATU	Q94546	0.54	0.54	416	Y	Q94546_416
TYIDPETYEDPNR	VDPETYE	EPHA7_607_619	SGPL_DROME	SGPL	Q9V7Y2	0.54	0.46	255	Y	Q9V7Y2_255
TIEENVYEVEEPN	IEGEVYEVDE	HAVR2_266_278	YS11_DROME	YS11	Q9W0Y2	0.77	0.54	75	Y	Q9W0Y2_75
DVHNLDYYKKTTN	DIQSTDYYRKNTN	FGFR3_641_653	FGFR1_DROME	FGFR1	Q07407	1	0.54	586	Y	Q07407_586
DVHNLDYYKKTTN	DIQSTDYYRKNTN	FGFR3_641_653	FGFR1_DROME	FGFR1	Q07407	1	0.54	587	Y	Q07407_587
KVDNEDIYESRHE	DDEDIYE	FRK_380_392	BL1S3_DROME	BL1S3	A8JNV4	0.54	0.46	40	Y	A8JNV4_40
SLGFKRSYEEHIP	YEEHIP	INSR_1348_1360	CLU_DROME	CLU	A1ZAB5	0.46	0.46	435	Y	A1ZAB5_435
EEGAPDYENLQEL	PDYQNLPEL	LAT_249_261	SHARK_DROME	SHARK	Q24145	0.69	0.54	927	Y	Q24145_927
EEGAPDYENLQEL	AADYDTLQEL	LAT_249_261	ATC1_DROME	ATC1	P22700	0.77	0.54	407	Y	P22700_407
HTGFLTEYVATRW	HTGFLTEYVATRW	MK01_180_192	ERKA_DROME	ERKA	P40417	1	1	200	Y	P40417_200
HTGFLTEYVATRW	GMLTDYVATRW	MK01_180_192	ERK7_DROME	ERK7	Q9W354	0.85	0.69	192	Y	Q9W354_192
HTGFLTEYVATRW	MTGYVATRW	MK01_180_192	MK38A_DROME	MK38A	O62618	0.69	0.54	186	Y	O62618_186
HTGFLTEYVATRW	MTGYVATRW	MK01_180_192	MK38B_DROME	MK38B	O61443	0.69	0.54	185	Y	O61443_185
EKLGEGSYGSVYKAI	KLGEGSYGSVYKAV	MST1_34_48_Y41	HIPPO_DROME	HIPPO	Q8T0S6	0.93	0.87	53	Y	Q8T0S6_53
EKLGEGSYGSVYKAI	KLGEGSYGSVYKAV	MST1_34_48_Y41	HIPPO_DROME	HIPPO	Q8T0S6	0.93	0.87	57	Y	Q8T0S6_57
EKLGEGSYGSVYKAI	EKIGEGTYGIVYKA	MST1_34_48_Y41	CDK2_DROME	CDK2	P23573	0.93	0.73	19	Y	P23573_19
EKLGEGSYGSVYKAI	EKIGEGTYGIVYKA	MST1_34_48_Y41	CDK2_DROME	CDK2	P23573	0.93	0.73	23	Y	P23573_23
EKLGEGSYGSVYKAI	EKIGEGTYGVVYK	MST1_34_48_Y41	CDK1_DROME	CDK1	P23572	0.87	0.67	15	Y	P23572_15
EKLGEGSYGSVYKAI	EKIGEGTYGVVYK	MST1_34_48_Y41	CDK1_DROME	CDK1	P23572	0.87	0.67	19	Y	P23572_19
EKLGEGSYGSVYKAI	EKIGEGTYGTVFK	MST1_34_48_Y41	CDK5_DROME	CDK5	P48609	0.87	0.6	15	Y	P48609_15
EKLGEGSYGSVYKAI	GEGTYGQVYKA	MST1_34_48_Y41	CDK12_DROME	CDK12	Q9VP22	0.73	0.6	815	Y	Q9VP22_815
EKLGEGSYGSVYKAI	GEGTYGQVYKA	MST1_34_48_Y41	CDK12_DROME	CDK12	Q9VP22	0.73	0.6	819	Y	Q9VP22_819
EKLGEGSYGSVYKAI	EELGEGAFGKVYK	MST1_34_48_Y41	ROR1_DROME	ROR1	Q24488	0.87	0.6	425	Y	Q24488_425
EKLGEGSYGSVYKAI	EKVGHGSYGVVCKAV	MST1_34_48_Y41	M3K7_DROME	M3K7	Q9V3Q6	1	0.67	30	Y	Q9V3Q6_30
EKLGEGSYGSVYKAI	GQGSFGCVYKA	MST1_34_48_Y41	FUSED_DROME	FUSED	P23647	0.73	0.53	19	Y	P23647_19
TSFMMTPYVVTRY	TTFMMTPYVVTRY	MK10_216_228	JNK_DROME	JNK	P92208	1	0.92	183	Y	P92208_183
TSFMMTPYVVTRY	TTFMMTPYVVTRY	MK10_216_228	JNK_DROME	JNK	P92208	1	0.92	188	Y	P92208_188
MQQSDSPYVVKYYGS	MQQCDSPYVVRYYGS	MST2_74_88_Y81	HIPPO_DROME	HIPPO	Q8T0S6	1	0.87	96	Y	Q8T0S6_96
MQQSDSPYVVKYYGS	MQQCDSPYVVRYYGS	MST2_74_88_Y81	HIPPO_DROME	HIPPO	Q8T0S6	1	0.87	100	Y	Q8T0S6_100
MQQSDSPYVVKYYGS	MQQCDSPYVVRYYGS	MST2_74_88_Y81	HIPPO_DROME	HIPPO	Q8T0S6	1	0.87	101	Y	Q8T0S6_101
GMSRDVYSTDYYR	GMTRDIYETDYYR	NTRK2_696_708	INSR_DROME	INSR	P09208	1	0.77	1545	Y	P09208_1545
GMSRDVYSTDYYR	GMTRDIYETDYYR	NTRK2_696_708	INSR_DROME	INSR	P09208	1	0.77	1549	Y	P09208_1549
GMSRDVYSTDYYR	GMTRDIYETDYYR	NTRK2_696_708	INSR_DROME	INSR	P09208	1	0.77	1550	Y	P09208_1550
GMSRDVYSTDYYR	GLSRDIYSSDYYR	NTRK2_696_708	ROR1_DROME	ROR1	Q24488	1	0.77	565	Y	Q24488_565
GMSRDVYSTDYYR	GLSRDIYSSDYYR	NTRK2_696_708	ROR1_DROME	ROR1	Q24488	1	0.77	569	Y	Q24488_569
GMSRDVYSTDYYR	GLSRDIYSSDYYR	NTRK2_696_708	ROR1_DROME	ROR1	Q24488	1	0.77	570	Y	Q24488_570
GMSRDVYSTDYYR	GLARDIYKSDYYR	NTRK2_696_708	7LESS_DROME	7LESS	P13368	1	0.62	2376	Y	P13368_2376
GMSRDVYSTDYYR	GLARDIYKSDYYR	NTRK2_696_708	7LESS_DROME	7LESS	P13368	1	0.62	2380	Y	P13368_2380
GMSRDVYSTDYYR	GLARDIYKSDYYR	NTRK2_696_708	7LESS_DROME	7LESS	P13368	1	0.62	2381	Y	P13368_2381
GMSRDVYSTDYYR	GLARDIQSTDYYR	NTRK2_696_708	FGFR1_DROME	FGFR1	Q07407	1	0.69	586	Y	Q07407_586
GMSRDVYSTDYYR	GLARDIQSTDYYR	NTRK2_696_708	FGFR1_DROME	FGFR1	Q07407	1	0.69	587	Y	Q07407_587
EPHVTRRTPDYFL	EPHVTRRTPDYFL	PP2AB_297_309	PP2A_DROME	PP2A	P23696	1	1	307	Y	P23696_307
SKRKGHEYTNIKY	GREYTNIKY	PTN11_539_551	CSW_DROME	CSW	P29349	0.69	0.62	666	Y	P29349_666
SKRKGHEYTNIKY	GREYTNIKY	PTN11_539_551	CSW_DROME	CSW	P29349	0.69	0.62	671	Y	P29349_671
KKIYNGDYYRQGR	IYSSDYYR	UFO_695_707	ROR1_DROME	ROR1	Q24488	0.62	0.46	565	Y	Q24488_565
KKIYNGDYYRQGR	IYSSDYYR	UFO_695_707	ROR1_DROME	ROR1	Q24488	0.62	0.46	569	Y	Q24488_569
KKIYNGDYYRQGR	IYSSDYYR	UFO_695_707	ROR1_DROME	ROR1	Q24488	0.62	0.46	570	Y	Q24488_570
KKIYNGDYYRQGR	IYKSDYYR	UFO_695_707	7LESS_DROME	7LESS	P13368	0.62	0.46	2376	Y	P13368_2376
KKIYNGDYYRQGR	IYKSDYYR	UFO_695_707	7LESS_DROME	7LESS	P13368	0.62	0.46	2380	Y	P13368_2380
KKIYNGDYYRQGR	IYKSDYYR	UFO_695_707	7LESS_DROME	7LESS	P13368	0.62	0.46	2381	Y	P13368_2381
TVDGKEIYNTIRR	DGKETYNAI	RASA1_453_465	BRCA2_DROME	BRCA2	Q9W157	0.69	0.54	546	Y	Q9W157_546
KIYSGDYYRQGCA	IYSSDYYR	TYRO3_679_691	ROR1_DROME	ROR1	Q24488	0.62	0.54	565	Y	Q24488_565
KIYSGDYYRQGCA	IYSSDYYR	TYRO3_679_691	ROR1_DROME	ROR1	Q24488	0.62	0.54	569	Y	Q24488_569
KIYSGDYYRQGCA	IYSSDYYR	TYRO3_679_691	ROR1_DROME	ROR1	Q24488	0.62	0.54	570	Y	Q24488_570
DFGLARDIYKNPD	DFGLARDIYK	VGFR1_1040_1052	7LESS_DROME	7LESS	P13368	0.77	0.77	2376	Y	P13368_2376
DFGLARDIYKNPD	DFGLSRDIY	VGFR1_1040_1052	ROR1_DROME	ROR1	Q24488	0.69	0.62	565	Y	Q24488_565
DFGLARDIYKNPD	DFGLSRDVY	VGFR1_1040_1052	TORSO_DROME	TORSO	P18475	0.69	0.54	767	Y	P18475_767
DFGLARDIYKNPD	DFGMTRDIY	VGFR1_1040_1052	INSR_DROME	INSR	P09208	0.69	0.54	1545	Y	P09208_1545
DFGLARDIYKNPD	DFGLAREMY	VGFR1_1040_1052	M3KSL_DROME	M3KSL	Q95UN8	0.69	0.54	298	Y	Q95UN8_298
DFGLARDIYKDPD	DFGLARDIYK	VGFR2_1046_1058	7LESS_DROME	7LESS	P13368	0.77	0.77	2376	Y	P13368_2376
DFGLARDIYKDPD	DFGLSRDIY	VGFR2_1046_1058	ROR1_DROME	ROR1	Q24488	0.69	0.62	565	Y	Q24488_565
DFGLARDIYKDPD	DFGLSRDVY	VGFR2_1046_1058	TORSO_DROME	TORSO	P18475	0.69	0.54	767	Y	P18475_767
DFGLARDIYKDPD	DFGMTRDIY	VGFR2_1046_1058	INSR_DROME	INSR	P09208	0.69	0.54	1545	Y	P09208_1545
DFGLARDIYKDPD	DFGLAREMY	VGFR2_1046_1058	M3KSL_DROME	M3KSL	Q95UN8	0.69	0.54	298	Y	Q95UN8_298
LRTHNGASPYQCT	LRTHSGERPYRC	ZBT16_621_633	GLAS_DROME	GLAS	P13360	0.92	0.62	465	Y	P13360_465
LRTHNGASPYQCT	LRIHSGEKPYQC	ZBT16_621_633	GLAS_DROME	GLAS	P13360	0.92	0.62	549	Y	P13360_549
LRTHNGASPYQCT	MRTHSGERPYRC	ZBT16_621_633	GLAS_DROME	GLAS	P13360	0.92	0.54	521	Y	P13360_521
LRTHNGASPYQCT	LRTHTGEQPYPC	ZBT16_621_633	HAM_DROME	HAM	Q8I7Z8	0.92	0.62	845	Y	Q8I7Z8_845
LRTHNGASPYQCT	RTHTGEKPYHC	ZBT16_621_633	KRH1_DROME	KRH1	P08155	0.85	0.54	413	Y	P08155_413
TSNQEYLDLSMPL	NDAYLDLSMPM	FGFR1_761_773	FGFR2_DROME	FGFR2	Q09147	0.85	0.62	1011	Y	Q09147_1011
ADSEMTGYVVTRW	AESEMTGYVATRW	MK12_178_190	MK38B_DROME	MK38B	O61443	1	0.85	185	Y	O61443_185
ADSEMTGYVVTRW	ENEMTGYVATRW	MK12_178_190	MK38A_DROME	MK38A	O62618	0.92	0.69	186	Y	O62618_186
ADSEMTGYVVTRW	MTPYVVTR	MK12_178_190	JNK_DROME	JNK	P92208	0.62	0.54	183	Y	P92208_183
RHTDDEMTGYVAT	RPTENEMTGYVAT	MK14_173_185	MK38A_DROME	MK38A	O62618	1	0.77	186	Y	O62618_186
RHTDDEMTGYVAT	RPAESEMTGYVAT	MK14_173_185	MK38B_DROME	MK38B	O61443	1	0.69	185	Y	O61443_185
RYFLDDQYTSSSG	RYVLDDQYTSSGG	TEC_512_524	BTKL_DROME	BTKL	P08630	1	0.85	671	Y	P08630_671
RYFLDDQYTSSSG	RYVLDDQYTSSGG	TEC_512_524	BTKL_DROME	BTKL	P08630	1	0.85	677	Y	P08630_677
ISLDNPDYQQDFF	SLDYPNYQQ	EGFR_1165_1177	SPR_DROME	SPR	Q8SWR3	0.69	0.54	56	Y	Q8SWR3_56
ISLDNPDYQQDFF	SLDYPNYQQ	EGFR_1165_1177	SPR_DROME	SPR	Q8SWR3	0.69	0.54	59	Y	Q8SWR3_59
KATGRYYAMKILK	YYAMKIL	AKT1_170_182	KAPC1_DROME	KAPC1	P12370	0.54	0.54	71	Y	P12370_71
KATGRYYAMKILK	YYAMKIL	AKT1_170_182	KAPC1_DROME	KAPC1	P12370	0.54	0.54	72	Y	P12370_72
KATGRYYAMKILK	KATAKLYAIKILK	AKT1_170_182	AKT1_DROME	AKT1	Q8INB9	1	0.69	292	Y	Q8INB9_292
KATGRYYAMKILK	KDTGHVYAMKVL	AKT1_170_182	TRC_DROME	TRC	Q9NBK5	0.92	0.62	119	Y	Q9NBK5_119
FSGTPEYLAPEVL	FCGTPEYLAPEVL	AKT1_309_321_C310S	AKT1_DROME	AKT1	Q8INB9	1	0.92	430	Y	Q8INB9_430
FSGTPEYLAPEVL	FAGTPGYLSPEVL	AKT1_309_321_C310S	KCC2A_DROME	KCC2A	Q00168	1	0.77	180	Y	Q00168_180
FSGTPEYLAPEVL	FCGTPEYVAPEV	AKT1_309_321_C310S	KGP24_DROME	KGP24	Q03043	0.92	0.77	941	Y	Q03043_941
FSGTPEYLAPEVL	FCGTPEYVAPEV	AKT1_309_321_C310S	KGP25_DROME	KGP25	P32023	0.92	0.77	787	Y	P32023_787
FSGTPEYLAPEVL	GTPEYLAPEI	AKT1_309_321_C310S	KAPC2_DROME	KAPC2	P16911	0.77	0.69	206	Y	P16911_206
FSGTPEYLAPEVL	GTPEYLAPEI	AKT1_309_321_C310S	KAPC1_DROME	KAPC1	P12370	0.77	0.69	207	Y	P12370_207
FSGTPEYLAPEVL	FCGTPDYIAPEIL	AKT1_309_321_C310S	KPC3_DROME	KPC3	P13678	1	0.69	573	Y	P13678_573
FSGTPEYLAPEVL	FCGTPEYVAPEI	AKT1_309_321_C310S	KGP1_DROME	KGP1	Q03042	0.92	0.69	622	Y	Q03042_622
FSGTPEYLAPEVL	GTPDYVAPEVL	AKT1_309_321_C310S	DAPKR_DROME	DAPKR	Q0KHT7	0.85	0.69	203	Y	Q0KHT7_203
FSGTPEYLAPEVL	FCGTPNYIAPEIL	AKT1_309_321_C310S	APKC_DROME	APKC	A1Z9X0	1	0.69	429	Y	A1Z9X0_429
FSGTPEYLAPEVL	GTPNYIAPEVL	AKT1_309_321_C310S	WARTS_DROME	WARTS	Q9VA38	0.85	0.69	927	Y	Q9VA38_927
FSGTPEYLAPEVL	FCGTPDYMAPEI	AKT1_309_321_C310S	KPC4_DROME	KPC4	P83099	0.92	0.62	508	Y	P83099_508
FSGTPEYLAPEVL	FCGTPDYIAPEI	AKT1_309_321_C310S	KPC1_DROME	KPC1	P05130	0.92	0.62	515	Y	P05130_515
FSGTPEYLAPEVL	GTPEYIAPEI	AKT1_309_321_C310S	KAPC3_DROME	KAPC3	P16912	0.77	0.62	435	Y	P16912_435
FSGTPEYLAPEVL	GTPDYIAPEV	AKT1_309_321_C310S	TRC_DROME	TRC	Q9NBK5	0.77	0.62	299	Y	Q9NBK5_299
FSGTPEYLAPEVL	FCGTPNYMAPEI	AKT1_309_321_C310S	KPC2_DROME	KPC2	P13677	0.92	0.62	536	Y	P13677_536
FSGTPEYLAPEVL	GTPNYIAPEIL	AKT1_309_321_C310S	POLO_DROME	POLO	P52304	0.85	0.62	189	Y	P52304_189
FSGTPEYLAPEVL	GTPDYISPEIL	AKT1_309_321_C310S	GEK_DROME	GEK	Q9W1B0	0.85	0.54	266	Y	Q9W1B0_266
VLEDNDYGRAVDW	VLDDNDYGQAVDW	AKT1_320_332	AKT1_DROME	AKT1	Q8INB9	1	0.85	441	Y	Q8INB9_441
KIGEGTYGVVYKG	KIGEGTYGVVYKG	CDK1_9_21	CDK1_DROME	CDK1	P23572	1	1	15	Y	P23572_15
KIGEGTYGVVYKG	KIGEGTYGVVYKG	CDK1_9_21	CDK1_DROME	CDK1	P23572	1	1	19	Y	P23572_19
KIGEGTYGVVYKG	KIGEGTYGIVYK	CDK1_9_21	CDK2_DROME	CDK2	P23573	0.92	0.85	19	Y	P23573_19
KIGEGTYGVVYKG	KIGEGTYGIVYK	CDK1_9_21	CDK2_DROME	CDK2	P23573	0.92	0.85	23	Y	P23573_23
KIGEGTYGVVYKG	KIGEGTYGTVFKG	CDK1_9_21	CDK5_DROME	CDK5	P48609	1	0.85	15	Y	P48609_15
KIGEGTYGVVYKG	IGEGTYGQVYK	CDK1_9_21	CDK12_DROME	CDK12	Q9VP22	0.85	0.77	815	Y	Q9VP22_815
KIGEGTYGVVYKG	IGEGTYGQVYK	CDK1_9_21	CDK12_DROME	CDK12	Q9VP22	0.85	0.77	819	Y	Q9VP22_819
KIGEGTYGVVYKG	IEEGTYGVVY	CDK1_9_21	KP58_DROME	KP58	Q9VPC0	0.77	0.69	569	Y	Q9VPC0_569
KIGEGTYGVVYKG	IEEGTYGVVY	CDK1_9_21	KP58_DROME	KP58	Q9VPC0	0.77	0.69	573	Y	Q9VPC0_573
KIGEGTYGVVYKG	KLGEGSYGSVYK	CDK1_9_21	HIPPO_DROME	HIPPO	Q8T0S6	0.92	0.69	53	Y	Q8T0S6_53
KIGEGTYGVVYKG	KLGEGSYGSVYK	CDK1_9_21	HIPPO_DROME	HIPPO	Q8T0S6	0.92	0.69	57	Y	Q8T0S6_57
KIGEGTYGVVYKG	IGEGSYSIVY	CDK1_9_21	PDPK1_DROME	PDPK1	Q9W0V1	0.77	0.54	257	Y	Q9W0V1_257
KIGEGTYGVVYKG	IGEGSYSIVY	CDK1_9_21	PDPK1_DROME	PDPK1	Q9W0V1	0.77	0.54	261	Y	Q9W0V1_261
KIGEGTYGVVYKG	KIGEGAYGEVF	CDK1_9_21	HASP_DROME	HASP	P83103	0.85	0.62	259	Y	P83103_259
KIGEGTYGVVYKG	KVGRGTYGHVYK	CDK1_9_21	CDK8_DROME	CDK8	Q9VT57	0.92	0.69	32	Y	Q9VT57_32
KIGEGTYGVVYKG	KVGRGTYGHVYK	CDK1_9_21	CDK8_DROME	CDK8	Q9VT57	0.92	0.69	36	Y	Q9VT57_36
EIGVGAYGTVYKA	IGEGTYGIVYKA	CDK4_11_23	CDK2_DROME	CDK2	P23573	0.92	0.69	19	Y	P23573_19
EIGVGAYGTVYKA	IGEGTYGIVYKA	CDK4_11_23	CDK2_DROME	CDK2	P23573	0.92	0.69	23	Y	P23573_23
EIGVGAYGTVYKA	QIGEGTYGQVYKA	CDK4_11_23	CDK12_DROME	CDK12	Q9VP22	1	0.69	815	Y	Q9VP22_815
EIGVGAYGTVYKA	QIGEGTYGQVYKA	CDK4_11_23	CDK12_DROME	CDK12	Q9VP22	1	0.69	819	Y	Q9VP22_819
EIGVGAYGTVYKA	IGRGHYGTVYK	CDK4_11_23	JAK_DROME	JAK	Q24592	0.85	0.69	903	Y	Q24592_903
EIGVGAYGTVYKA	IGRGHYGTVYK	CDK4_11_23	JAK_DROME	JAK	Q24592	0.85	0.69	907	Y	Q24592_907
EIGVGAYGTVYKA	IGSGSFGTVYRA	CDK4_11_23	KRAF1_DROME	KRAF1	P11346	0.92	0.62	444	Y	P11346_444
EIGVGAYGTVYKA	GSYGSVYKA	CDK4_11_23	HIPPO_DROME	HIPPO	Q8T0S6	0.69	0.54	53	Y	Q8T0S6_53
EIGVGAYGTVYKA	GSYGSVYKA	CDK4_11_23	HIPPO_DROME	HIPPO	Q8T0S6	0.69	0.54	57	Y	Q8T0S6_57
EIGVGAYGTVYKA	IGEGTYGTVFK	CDK4_11_23	CDK5_DROME	CDK5	P48609	0.85	0.62	15	Y	P48609_15
GEPNVSYISSRYY	GEPNVSYICSRYY	GSK3B_210_222_C218S	SGG_DROME	SGG	P18431	1	0.92	214	Y	P18431_214
GEPNVSYISSRYY	GEPNVSYICSRYY	GSK3B_210_222_C218S	SGG_DROME	SGG	P18431	1	0.92	219	Y	P18431_219
GEPNVSYISSRYY	GEPNVSYICSRYY	GSK3B_210_222_C218S	SGG_DROME	SGG	P18431	1	0.92	220	Y	P18431_220
GEPNVSYISSRYY	GEPNVSYICSRYY	GSK3B_210_222_C218S	GSK3H_DROME	GSK3H	P83101	1	0.92	193	Y	P83101_193
GEPNVSYISSRYY	GEPNVSYICSRYY	GSK3B_210_222_C218S	GSK3H_DROME	GSK3H	P83101	1	0.92	198	Y	P83101_198
GEPNVSYISSRYY	GEPNVSYICSRYY	GSK3B_210_222_C218S	GSK3H_DROME	GSK3H	P83101	1	0.92	199	Y	P83101_199
ALQTPSYTPYYVA	LQTPCYTPYYVA	MAPK3_198_210_C203S	MAPK2_DROME	MAPK2	P49071	0.92	0.85	181	Y	P49071_181
ALQTPSYTPYYVA	LQTPCYTPYYVA	MAPK3_198_210_C203S	MAPK2_DROME	MAPK2	P49071	0.92	0.85	184	Y	P49071_184
ALQTPSYTPYYVA	LQTPCYTPYYVA	MAPK3_198_210_C203S	MAPK2_DROME	MAPK2	P49071	0.92	0.85	185	Y	P49071_185
LNSKGYTKSI	LNSKGYTKSI	MK01_200_209	ERKA_DROME	ERKA	P40417	1	1	218	Y	P40417_218
GFLTEYVATR	GFLTEYVATR	MK03_199_208	ERKA_DROME	ERKA	P40417	1	1	200	Y	P40417_200
GFLTEYVATR	GMLTDYVATR	MK03_199_208	ERK7_DROME	ERK7	Q9W354	1	0.8	192	Y	Q9W354_192
MMTPYVVTRYY	MMTPYVVTRYY	MK08_181_191	JNK_DROME	JNK	P92208	1	1	183	Y	P92208_183
MMTPYVVTRYY	MMTPYVVTRYY	MK08_181_191	JNK_DROME	JNK	P92208	1	1	188	Y	P92208_188
MMTPYVVTRYY	MMTPYVVTRYY	MK08_181_191	JNK_DROME	JNK	P92208	1	1	189	Y	P92208_189
DEMTGYVATRW	NEMTGYVATRW	MK14_177_187	MK38A_DROME	MK38A	O62618	1	0.91	186	Y	O62618_186
DEMTGYVATRW	EMTGYVATRW	MK14_177_187	MK38B_DROME	MK38B	O61443	0.91	0.91	185	Y	O61443_185
DEMTGYVATRW	LTEYVATRW	MK14_177_187	ERKA_DROME	ERKA	P40417	0.82	0.64	200	Y	P40417_200
RKGHEYTNIKY	GREYTNIKY	PTN11_541_551	CSW_DROME	CSW	P29349	0.82	0.73	666	Y	P29349_666
RKGHEYTNIKY	GREYTNIKY	PTN11_541_551	CSW_DROME	CSW	P29349	0.82	0.73	671	Y	P29349_671
QNTGDYYDLYG	QNNGDFFDLYG	PTN11_57_67	CSW_DROME	CSW	P29349	1	0.73	66	Y	P29349_66
KHKEDVYENLHTK	VYENLHT	PTN6_558_570	TRIB_DROME	TRIB	Q9V3Z1	0.54	0.54	223	Y	Q9V3Z1_223
LQERRKYLKHRLI	LMQRRKYLKLR	STAT2_684_696	MY61F_DROME	MY61F	Q23979	0.85	0.62	726	Y	Q23979_726
