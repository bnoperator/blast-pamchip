PepProtein_SeqMatch	Sequence match of the peptide to the protein of origin (PepProtein) of peptide
PepProtein_SeqIdentity	Sequence identity of the peptide to the protein of origin (PepProtein) of peptide
PepProtein_UniprotACC	Uniprot accession of the peptide to the protein of origin (PepProtein) of peptide
PepProtein_UniprotName	Uniprot name match of the peptide to the protein of origin (PepProtein) of peptide
PepProtein_UniprotID	UniprotID match of the peptide to the protein of origin (PepProtein) of peptide
PepProtein_PhosSite	Phopho site position of the residue of the peptide in the protein of origin (PepProtein) of peptide
PepProtein_Residue	Residue (Y,S,T) of the peptide to the protein in the origin (PepProtein) of peptide
PepProtein_PhosLink	Phospho link, unique combination of the UniprotID and phospho position of the PepProtein

Kinase_UniprotName	Uniprot name of the kinase
Kinase_UniprotID	Uniprot ID of the kinase
Kinase_Group	kinase group
Kinase_Family	kinase family
Kinase_Subfamily	kinase subfamily
Kinase_Synonym	kinase name synoym
PubMed_ID	PubMed Id indicating the Kinase relationship
PubMed_Count	Total number of PubMed Identifiers which indicate the Kinase relationship
Kinase_KinexusRank	Kinase Kinexus Rank
