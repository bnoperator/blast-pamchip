blast<- function(product) {

  convert_to_fasta(product$array_layout_file_path)
  add_dummy_entry(product$array_layout_fasta_file_path)
  
  blastp(blast_setting, product)
  
  queryTable <-
    read.delim2(blast_setting$out_file_path, stringsAsFactors = FALSE)
  
  IDs <- get_IDs(product)
  
  queryTable <- removedummy(queryTable)
  # queryTable <- removeGapEntries(queryTable)
  queryTable <- keepEntriesSwissProt(queryTable)
  
  isthere <- queryTable$sseq =="AILRRPTSPVSRE"
  which(isthere)
  queryTable$sseq[which(isthere)]
  
  if ((product$category == "PTK") || (product$category == "PTP")) {
    queryTable <- keepEntriesWithResidue("Y", queryTable)
    queryTable <- addResiduePos(c("Y"), queryTable, outfile)
    queryTable <- removeNoPhosphoSite(queryTable)
  }
  
  if (product$category == "STK") {
    KPCB_19_31_A25S_Table <-
      keepEntriesWithID(queryTable, "KPCB_19_31_A25S")
    queryTable <- keepEntriesWithResidue("T|S", queryTable)
    queryTable <- rbind(KPCB_19_31_A25S_Table, queryTable)
    queryTable <- addResiduePos(c("S", "T"), queryTable, outFile)
    
    KPCB_19_31_A25S_Table <-
      keepEntriesWithID(queryTable, "KPCB_19_31_A25S")
    queryTable <- removeNoPhosphoSite(queryTable)
    queryTable <- rbind(KPCB_19_31_A25S_Table, queryTable)
  }
  
  
  queryTable <- calculateSeqIdentity(queryTable)
  
  queryTable <- addPhosphoLink(queryTable)
  
  queryTable <- add_sequence(queryTable)
  queryTable <- add_ID(queryTable)
  queryTable <- add_organism(queryTable)
  queryTable <- addUniprotNames(queryTable)
  
  queryTable <- dupCol(queryTable, "sacc", "PepProtein_UniprotID")
  queryTable <- dupCol(queryTable, "sseq", "PepProtein_SeqMatch")
  queryTable <- calculateSeqSimilarity(queryTable)
  
  queryTable <-
    keep_entry(queryTable, "Organism" , product$organism)
  
  #queryTablePM<-keepPerfectMatches(queryTable)
  queryTableShort <-
    queryTable[c(
      "Sequence",
      "PepProtein_SeqMatch",
      "ID",
      "PepProtein_UniprotACC",
      "PepProtein_UniprotName",
      "PepProtein_UniprotID",
      "PepProtein_SeqSimilarity",
      "PepProtein_SeqIdentity",
      "PepProtein_PhosSite",
      "PepProtein_Residue",
      "PepProtein_PhosLink"
    )]
  queryTableShortSimilarity <- keepEntriesSimilarity(0.8, queryTableShort)
  
  outfile <-
    make_filename("Pep2Protein", "SimilarityAll", blast_setting, product)
  outfile_similarity <-
    make_filename("Pep2Protein", "Similarity80", blast_setting, product)
  outfile_short <-
    make_filename("Pep2Protein", "Short", blast_setting, product)
  # outfile_100 <-      make_filename("Pep2Protein", "100", blast_setting, product)
  write.table(
    file = outfile_similarity,
    queryTableShortSimilarity,
    row.names = FALSE,
    sep = "\t",
    quote = FALSE
  )
  write.table(
    file = outfile_short,
    queryTableShort,
    row.names = FALSE,
    sep = "\t",
    quote = FALSE
  )
  write.table(
    file = outfile,
    queryTable,
    row.names = FALSE,
    sep = "\t",
    quote = FALSE
  )
}

