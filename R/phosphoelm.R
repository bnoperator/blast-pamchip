library(dplyr)
library(tidyr)

# download.file("ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/idmapping/by_organism/HUMAN_9606_idmapping.dat.gz")
elm_file <- "F:\\Data\\enrichmentdbs\\phosphoELM-2015\\phosphoELM_vertebrate_2015-04.dump"
kinase_file <- "F:\\Data\\enrichmentdbs\\phosphoELM-2015\\elm_kinases.txt"
group_file <- "F:\\Data\\enrichmentdbs\\phosphoELM-2015\\elm_kinases_group.txt"

file.exists(elm_file)
file.exists(kinase_file)
file.exists(group_file)

id_file <- "F:\\Data\\enrichmentdbs\\uniport-Oct-2019\\HUMAN_9606_idmapping.dat"

file.exists(id_file)

elm     <- read.delim(elm_file, header = TRUE, stringsAsFactors = FALSE)
kinase  <- read.delim(kinase_file,  header = FALSE, stringsAsFactors = FALSE)
group   <- read.delim(group_file,  header = TRUE, stringsAsFactors = FALSE) 
id      <- read.delim(id_file,  header = FALSE, stringsAsFactors = FALSE) 

elm <- as_tibble(elm)
group <- as_tibble(group)
kinase <- as_tibble(kinase)
id  <- as_tibble(id)

id <- id %>% 
  rename(id_uniprotdb = V1,
         id_type     = V2, 
         id_value    = V3) %>%
  filter( id_type  == "Gene_Name") %>% 
  select(-id_type)

kinase <- kinase %>% 
  rename(elm_kinase_name = V1,
         elm_kinase_id   = V2) 

elm <- elm %>% 
  filter(species == "Homo sapiens" & kinases != "" & kinases != "NA") %>% 
  select(-sequence, -pmids, -source, -species, -entry_date)

elm <- left_join(x= elm, y= group, by= c("kinases" = "kinase_group"))
elm <- left_join(x= elm, y= kinase, by= c("kinases" = "elm_kinase_name"))

elm <- elm %>%
  mutate(kinase_acc = coalesce(kinase_id, elm_kinase_id))
elm <- elm %>%
  mutate(kinase_name  = coalesce(kinase, kinases))
elm <- elm %>%
  select(acc, position, code, kinase_acc, kinase_name)
elm <- elm %>%
  left_join(y= id, by= c("acc" = "id_uniprotdb"))
elm <- elm %>%
  left_join(y =id, by= c("kinase_acc" = "id_uniprotdb"), suffix = c(".substrate", ".kinase"))


  elm <- elm %>% 
    rename(Kinase_UniprotName      = id_value.kinase,
           Kinase_UniprotID        = kinase_acc, 
           Protein_UniprotName  = id_value.substrate, 
           Protein_UniprotID    = acc, 
           Protein_PhosSite     = position, 
           Protein_Residue      = code) %>%
    mutate(Protein_PhosLink = paste(Protein_UniprotID, Protein_PhosSite, sep = '_')) %>%
    select(Kinase_UniprotName,
           Kinase_UniprotID,
           Protein_UniprotName,
           Protein_UniprotID,
           Protein_PhosSite,
           Protein_Residue,
           Protein_PhosLink ) %>%
    mutate(Database = "PHOSPHOELM")
  
  write.table(elm, file = "results_kinases\\phosphoelm.txt", row.names= FALSE, sep = "\t")
