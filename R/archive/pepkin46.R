########## PTK ###############################################
arrayLayout<-"pepkin46 Array Layout.txt"
arrayLayoutFilePath<-paste(organismDir,sep="\\",arrayLayout)
convertToFasta(arrayLayoutFilePath)
arrayLayoutFasta<-paste(organismDir,sep="\\","pepkin46-fasta.txt")
arrayType<-"PTK-pepkin46"
#remove the CX


outFile<-makeFileName(base,"Pep2Protein","Full","PTK","pepkin46","HUMAN","UniProt")
outFile100<-makeFileName(base,"Pep2Protein","100","PTK","pepkin46","HUMAN","UniProt")
outFileIdentity<-makeFileName(base,"Pep2Protein","Homology","PTK","pepkin46","HUMAN","UniProt")
outFileShort<-makeFileName(base,"Pep2Protein","Short","PTK","pepkin46","HUMAN","UniProt")


blastp("HUMAN", arrayLayoutFasta, arrayType,organismDir, blastdbDir)
outFileName<-"query.out"
queryTable<-read.delim2(outFileName, stringsAsFactors=FALSE)

IDs<-getIDs(organismDir, arrayLayout)

queryTable<-removeGapEntries(queryTable)
queryTable<-keepEntriesSwissProt(queryTable)


queryTable<-keepEntriesWithResidue("Y",queryTable)
queryTable<-addResiduePos(c("Y"),queryTable,outFile)
queryTable<-removeNoPhosphoSite(queryTable)

queryTable<-calculateSeqHomology(queryTable)

queryTable<-addPhosphoLink(queryTable)
queryTable<-addSequence(queryTable)
queryTable<-addID(queryTable)
queryTable<-addUniprotNames(queryTable)

queryTable<-dupCol( queryTable,"sacc","PepProtein_UniprotID")
queryTable<-dupCol( queryTable,"sseq","PepProtein_SeqMatch")

#queryTablePM<-keepPerfectMatches(queryTable)
queryTableShort<-queryTable[c("Sequence","PepProtein_SeqMatch","ID","PepProtein_UniprotACC","PepProtein_UniprotName","PepProtein_UniprotID", "PepProtein_SeqHomology","PepProtein_PhosSite","PepProtein_Residue","PepProtein_PhosLink")]
queryTableShortHomology<-keepEntriesHomology(0.8,queryTableShort)

write.table(file=outFileIdentity, queryTableShortHomology, row.names=FALSE, sep="\t",quote=FALSE)
write.table(file=outFileShort, queryTableShort, row.names=FALSE, sep="\t",quote=FALSE)
write.table(file=outFile, queryTable, row.names=FALSE, sep="\t",quote=FALSE)
