library(dplyr)
library(tidyr)
library(readr)

phos_ptm_file <- "F:\\Data\\enrichmentdbs\\PhosphositePlus-May-2019\\Kinase_Substrate_Dataset"
file.exists(phos_ptm_file)
id_file <- "F:\\Data\\enrichmentdbs\\uniport-Oct-2019\\HUMAN_9606_idmapping.dat"
file.exists(id_file)

ptm <- read_delim(phos_ptm_file,"\t", escape_double = FALSE, trim_ws = TRUE, skip = 3)
# ptm <- read.delim(phos_ptm_file, header = TRUE, stringsAsFactors = FALSE, skip = -3L)
id      <- read.delim(id_file,  header = FALSE, stringsAsFactors = FALSE) 
id <- id %>% 
  rename(id_uniprotdb = V1,
         id_type     = V2, 
         id_value    = V3) %>%
  filter( id_type  == "Gene_Name") %>% 
  select(-id_type)


phos_ptm <- ptm %>% 
  select(KINASE, KIN_ACC_ID, KIN_ORGANISM, SUB_GENE, SUB_ACC_ID, SUB_ORGANISM, SUB_MOD_RSD) %>%
  filter(KIN_ORGANISM == "human" & SUB_ORGANISM == "human") %>% 
  select(-KIN_ORGANISM, -SUB_ORGANISM)

# hprd <- left_join(x= phos_ptm, y= uni_id, by= c("enzyme_name" = "geneSymbol")) %>%
#   rename( kinase_id = swissprot_id) %>% 
#   separate_rows("kinase_id", sep=",") %>% 
#   left_join(y= uni_id, by= c("substrate_gene_symbol" = "geneSymbol")) %>%
#   rename( substrate_id = swissprot_id) %>% 
#   separate_rows("substrate_id", sep=",") %>% 
#   distinct

phos_ptm <- phos_ptm %>% 
  mutate(residue = substr(SUB_MOD_RSD, start=1, stop=1), 
            site    = substr(SUB_MOD_RSD, start=2, stop=nchar(SUB_MOD_RSD)))%>%
  select(-SUB_MOD_RSD)

phos_ptm <- phos_ptm %>%
  separate(
    KIN_ACC_ID,
    into = c("kinase_uniprotid", "kinase_isoform"),
    sep = "-")  

phos_ptm <- phos_ptm %>%
  separate(
    SUB_ACC_ID,
    into = c("substrate_uniprotid", "substrate_isoform"),
    sep = "-")       

phos_ptm <- phos_ptm %>%
  left_join(y= id, by= c("kinase_uniprotid" = "id_uniprotdb"))


phos_ptm <- phos_ptm %>% 
  rename(Kinase_UniprotName      = id_value,
         Kinase_UniprotID        = kinase_uniprotid, 
         Protein_UniprotName  = SUB_GENE, 
         Protein_UniprotID    = substrate_uniprotid, 
         Protein_PhosSite     = site, 
         Protein_Residue      = residue) %>%
  mutate(Protein_PhosLink = paste(Protein_UniprotID, Protein_PhosSite, sep = '_')) %>%
  select(Kinase_UniprotName,
         Kinase_UniprotID,
         Protein_UniprotName,
         Protein_UniprotID,
         Protein_PhosSite,
         Protein_Residue,
         Protein_PhosLink ) %>%
  mutate(Database = "PHOSPHOSITEPLUS")

write.table(phos_ptm, file = "results_kinases\\phosphositeplus.txt", row.names= FALSE, sep = "\t")

