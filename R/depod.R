library(dplyr)
library(tidyr)
library(readr)

depod_ptm_file <- "F:\\Data\\enrichmentdbs\\depod-2014\\DEPOD_201408_human_phosphatase-substrate.txt"
file.exists(depod_ptm_file)
substrate_file <- "results\\90183\\Pep2Protein-Homology-PTP-90183-swiss_prot-HUMAN-2019-05-08.txt"
file.exists(substrate_file)

ptm <- read.delim2(depod_ptm_file, stringsAsFactors = FALSE)

# ptm <- read.delim(phos_ptm_file, header = TRUE, stringsAsFactors = FALSE, skip = -3L)
# id      <- read.delim(id_file,  header = FALSE, stringsAsFactors = FALSE) 
# id <- id %>% 
#   rename(id_uniprotdb = V1,
#          id_type     = V2, 
#          id_value    = V3) %>%
#   filter( id_type  == "Gene_Name") %>% 
#   select(-id_type)
subs <- read.delim2(substrate_file, stringsAsFactors = FALSE)

dephos_ptm <- ptm %>% 
  select(Phosphatase, Substrate,Substrate.source.organism , Dephosphorylation.site) %>%
  filter(Substrate.source.organism == "Homo sapiens (Human)" )  %>%
  select(-Substrate.source.organism)

dephos_ptm <- dephos_ptm %>% separate_rows(Dephosphorylation.site, sep = ", ")

dephos_ptm <- dephos_ptm %>% filter(grepl("Tyr|N/A",Dephosphorylation.site))

# hprd <- left_join(x= phos_ptm, y= uni_id, by= c("enzyme_name" = "geneSymbol")) %>%
#   rename( kinase_id = swissprot_id) %>% 
#   separate_rows("kinase_id", sep=",") %>% 
#   left_join(y= uni_id, by= c("substrate_gene_symbol" = "geneSymbol")) %>%
#   rename( substrate_id = swissprot_id) %>% 
#   separate_rows("substrate_id", sep=",") %>% 
#   distinct

dephos_ptm <- dephos_ptm %>%
  separate(
    Dephosphorylation.site,
    into = c("Residue", "Residue Position"),
    sep = "-")  

dephos_ptm <- dephos_ptm %>% mutate(Residue = gsub(pattern="Tyr", replacement = "Y", Residue))
dephos_ptm <- dephos_ptm %>% mutate(Residue = gsub(pattern="N/A", replacement = "", Residue))

dephos_ptm <- dephos_ptm %>% separate_rows(`Residue Position`, sep = " ")

dephos_ptm <- dephos_ptm %>% mutate(`Residue Position`  = gsub(pattern="\\D", replacement = "", `Residue Position`))
dephos_ptm <- dephos_ptm %>% mutate(`Residue`  = gsub(pattern="[^Y]", replacement = "", `Residue`))
dephos_ptm <- dephos_ptm %>% distinct()

# 
# dephos_ptm <- dephos_ptm %>% 
#   mutate(residue = substr(SUB_MOD_RSD, start=1, stop=1), 
#             site    = substr(SUB_MOD_RSD, start=2, stop=nchar(SUB_MOD_RSD)))%>%
#   select(-SUB_MOD_RSD)
# 
# phos_ptm <- phos_ptm %>%
#   separate(
#     KIN_ACC_ID,
#     into = c("kinase_uniprotid", "kinase_isoform"),
#     sep = "-")  
# 
# phos_ptm <- phos_ptm %>%
#   separate(
#     SUB_ACC_ID,
#     into = c("substrate_uniprotid", "substrate_isoform"),
#     sep = "-")       

dephos_ptm <- dephos_ptm %>% mutate(`Residue Position`= as.numeric (dephos_ptm$`Residue Position`))

final_ptm <- subs %>%
  left_join(y= dephos_ptm, by= c("PepProtein_UniprotName"="Substrate", "PepProtein_PhosSite" = "Residue Position" ))

final_ptm <- final_ptm %>% select(-Residue)

# phos_ptm <- phos_ptm %>% 
#   rename(Kinase_UniprotName      = id_value,
#          Kinase_UniprotID        = kinase_uniprotid, 
#          Protein_UniprotName  = SUB_GENE, 
#          Protein_UniprotID    = substrate_uniprotid, 
#          Protein_PhosSite     = site, 
#          Protein_Residue      = residue) %>%
#   mutate(Protein_PhosLink = paste(Protein_UniprotID, Protein_PhosSite, sep = '_')) %>%
#   select(Kinase_UniprotName,
#          Kinase_UniprotID,
#          Protein_UniprotName,
#          Protein_UniprotID,
#          Protein_PhosSite,
#          Protein_Residue,
#          Protein_PhosLink ) %>%
#   mutate(Database = "PHOSPHOSITEPLUS")

write.table(final_ptm, file = "depod_ptp.txt", row.names= FALSE, sep = "\t")

