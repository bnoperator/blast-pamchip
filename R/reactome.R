library(dplyr)
library(tidyr)
library(stringr)
library(readr)

reactome_file <- "F:\\Data\\enrichmentdbs\\reactome-Mar-2018\\Homo_sapiens_phospho_site_2_pathway.csv"
file.exists(reactome_file)

id_file <- "F:\\Data\\enrichmentdbs\\uniport-Oct-2019\\HUMAN_9606_idmapping.dat"
file.exists(id_file)


react <- read_delim(reactome_file,"\t", escape_double = FALSE, trim_ws = TRUE)
id <- read.delim(id_file, header = FALSE, stringsAsFactors = FALSE)


id <- id %>%
  filter( V2 == "Gene_Name") %>% 
  select(-V2)

react <- react %>% 
  select(proteinName, uniprotId, phosphoSite, `UniPro Phosphosite`, phosphorylationType, kinase, kinaseuniprotId) %>%
  filter(!is.na(kinaseuniprotId)) %>% 
  filter(!is.na(uniprotId)) %>% 
  filter(phosphoSite > 0)

react <- react %>%
  separate(
    uniprotId,
    into = c("uid", "iso"),
    sep = "-",
    convert = TRUE
  )

react <- react %>%
  left_join(y =id, by= c("uid" = "V1"))

react <- react %>%
  separate(
    phosphorylationType,
    into = c("part1", "part2", "part3", "part4"),
    sep = "-",
    convert = TRUE
  )



react <- react %>%
  mutate(part4 = gsub("serine]", "S", part4), 
         part4 = gsub("threonine]", "T", part4),
         part4 = gsub("tyrosine]", "Y", part4))

react <- react %>% 
    distinct
            
            

react <- react %>% 
  rename(Kinase_UniprotName      = kinase,
         Kinase_UniprotID        = kinaseuniprotId, 
         Protein_UniprotName  = V3, 
         Protein_UniprotID    = uid, 
         Protein_PhosSite     = phosphoSite, 
         Protein_Residue      = part4,
         Protein_PhosLink     = `UniPro Phosphosite`) %>%
  select(Kinase_UniprotName,
         Kinase_UniprotID,
         Protein_UniprotName,
         Protein_UniprotID,
         Protein_PhosSite,
         Protein_Residue,
         Protein_PhosLink ) %>%
  mutate(Database = "REACTOME")

write.table(react, file = "results_kinases//REACTOME.txt", row.names= FALSE, sep = "\t")

