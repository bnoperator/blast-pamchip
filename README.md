#Intro
This repository contains the following code:

- ptk stk and ptp blasting.
- kinase extraction for a set of dbs

#Getting start

The main blasting code is in:
```
blastp-PTK-STK-PTP.R
```

The supporting functions are in:
```
functions.R
```

The code for downloading and the indexing the blast db is in:
```
makeblastdb.R
```

I have included the ncbi tool as part of the repository.
It is downloaded from the original location at:

```
https://ftp.ncbi.nlm.nih.gov/blast/executables/LATEST/
```

The script uses `blastp` setting (i.e. small protein sequences)

The `blastp` settings can be found at:

https://www.ncbi.nlm.nih.gov/books/NBK279684/

see Table C3
